
/**
 * Draw.io Plugin to create C4 Architecture Diagramms
 *
 * LOAD C4 SHAPE LIBRARY:
 *
 * https://raw.githubusercontent.com/tobiashochguertel/draw-io/master/C4-drawIO.xml
 */
Draw.loadPlugin(function (ui) {
    var sidebar_id = 'dept-c4';
    var sidebar_title = 'Dept C4 Notation';

    var c4Utils = {};
    c4Utils.isC4 = function (cell) {
        return (cell &&
            cell.hasOwnProperty('c4') &&
            (cell.c4 !== null));
    };
    c4Utils.isC4Model = function (cell) {
        return (c4Utils.isC4(cell) &&
            cell &&
            cell.hasOwnProperty('value') &&
            (cell.value &&
                cell.value.hasAttribute('c4Type'))
        );
    };
    c4Utils.isC4Person = function (cell) {
        return (c4Utils.isC4(cell) &&
            (cell.hasOwnProperty('value') &&
                cell.value.length === 0 ) &&
            cell.getChildCount() === 3 &&
            cell.getChildAt(2).value.getAttribute('c4Type') === 'body');
    };
    c4Utils.isC4SoftwareSystem = function (cell) {
        return (c4Utils.isC4(cell) &&
            cell.getAttribute('c4Type') === 'SoftwareSystem');
    };
    c4Utils.isC4Relationship = function (cell) {
        return (c4Utils.isC4(cell) &&
            cell.getAttribute('c4Type') === 'Relationship');
    };

    c4Utils.createSettingsIcon = function () {
        var img = mxUtils.createImage('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAXCAYAAADgKtSgAAACpklEQVRIS7VVMU9TURg9nWiZoF0k1MQmlKCREhhowUHaScpWdYHoINUORoKTiT+AhMnE6IDigraL2g10amGhxaFGMJHQJiWxBJcWJl6Zas4H9/leH4VKwl2a13vv+c73nfN911ar1Wq4oGVrBpzxq9VDnYLd3gKbzXYmpabAs2s5bBWKCAwOIPstJ7/dXs/5wNMrGfh6e+BytgvA4pcU3J0d6PNdRWp5FZpWxdhoSPbKlT2sb2wieHPIEszC/H08iQNNQ6m0i1DwBhwOu4BPP3kgwUo7u+CZ4MiwBMlkc3C52tDqcODeRMQUwAROVvlCEbHohFz8mFyUw2SpsuA3A/AsAblHAnPzcXi7PAiNDOsBTOBMce5tAk+nJuWCceUL2/qnt+uKaY9EXrx8h9jDcRMJS1nIqFLZx51IWAB+rP+SsjB11p2sy+V9YUwNuD4ll+B0tplY838LuHLG/YnbOnA9I5WhCrAQ/4zuLg8C/gFrzenjjZ+bKO38QWYtp4s3M/vakqq6rQI8f/ZYHPNmPoE+3zW4Oy+h93qP9IEwV+Ixutfrkbpt5YtIr6yKuI0W60z29DwD5PNF6Ye7kTHRTAf/Xdo1NQbB6Rzl55MCUAs6xNhQvHfZ3WEGpyhkTSecm3lhW9jTDDpz1pxdRifQHUrA/6k5LUz30FHsbr3mxpTr3bL0NYVHUbN/lYDhW0d2PNUtRvDGPm+XWlKbcnnP5POmwE/rUAqlVv1EpNtmZl9hemqycYcezZZtxKLjMlsoMld4NGiZLenljIj2b7YkxAwNZwuBmKKmHUrqAX8/WtVUPGZF0Rc+JBEaGcKBVkV27TtcrnY4HC1gVxvXiY8FM6BQzcxzBmPJjIxVgKZfIpaLs4Nu8g/2n/8lqu/GC31DGw6XMzb+An4I4cvYKbPGAAAAAElFTkSuQmCC');
        img.setAttribute('title', 'Settings');
        img.style.cursor = 'pointer';
        img.style.width = '16px';
        img.style.height = '16px';
        return img;
    };
    c4Utils.registCodec = function (func) {
        var codec = new mxObjectCodec(new func());
        codec.encode = function (enc, obj) {
            try {
                var data = enc.document.createElement(func.name);
            } catch (e) {
                (window.console && console.error(e));
            }
            return data
        };
        codec.decode = function (dec, node, into) {
            return new func();
        };
        mxCodecRegistry.register(codec);
    };

    // labels
    c4Utils.labelPerson = function(c4Name, c4Description)
    {
        return '<b><font style="font-size: 14px"><u>' + c4Name + '</u></font></b><div><font style="font-size: 10px">[Person]</font></div><div><br></div><div>' + c4Description + '</div>';
    };
    c4Utils.labelSoftwareSystem = function(c4Name, c4Technology, c4Description)
    {
        return c4Utils.labelGeneral(c4Name, 'Software System', c4Technology, c4Description);
    };
    c4Utils.labelContainer = function(c4Name, c4Technology, c4Description)
    {
        return c4Utils.labelGeneral(c4Name, 'Container', c4Technology, c4Description);
    };
    c4Utils.labelComponent = function(c4Name, c4Technology, c4Description)
    {
        return c4Utils.labelGeneral(c4Name, 'Component', c4Technology, c4Description);
    };
    c4Utils.labelGeneral = function(c4Name, c4Type, c4Technology, c4Description)
    {
        var result = '<b><font style="font-size: 14px"><u>' + c4Name + '</u></font></b>';
        if (c4Technology || 0 !== c4Technology.length)
        {
            result += '<div><font style="font-size: 10px">[' + c4Type + ':&nbsp;' + c4Technology + ']</font></div>';
        }
        else
        {
            result += '<div><font style="font-size: 10px">[' + c4Type + ']</font></div>';
        }
        result += '<div><br></div><div>' + c4Description + '</div>';

        return result;
    };
    c4Utils.labelRelationship = function(c4Description, c4Technology)
    {
        var result = '<div style="text-align: left"><div style="text-align: center"><b>' + c4Description + '</b></div>';
        if(c4Technology || 0 !== c4Technology.length)
        {
            result += '<div style="text-align: center">[' + c4Technology + ']</div>';
        }
        result += '</div>';
        return result;
    };
    c4Utils.labelExecutionEnvironment = function(c4Name, c4Application)
    {
        return '<u style="font-size: 12px">' + c4Name + '</u><br style="font-size: 12px"><span style="font-weight: normal ; font-size: 12px">[' + c4Application + ']</span>';
    };
    c4Utils.labelDeploymentNode = function(c4Name, c4OperationSystem, c4ScalingFactor)
    {
        return '<div style="text-align: left">' + c4Name + '</div><div style="text-align: left">[' + c4OperationSystem + ']</div><div style="text-align: right">' + c4ScalingFactor + '</div>';
    };
    c4Utils.labelDynamicRelationship = function(c4Step, c4Description, c4Technology)
    {
        return '<bold>' + c4Step + ': </bold><bold>' + c4Description + '</bold>' + '<div>[' + c4Technology + ']</div><div><br></div><div>';
    };
    c4Utils.labelDatabase = function(c4Technology, c4Description)
    {
        return '<span>Database</span><div>[Container:&nbsp;' + c4Technology + ']</div><div><br></div><div>' + c4Description + '</div>';
    };
    c4Utils.labelPersonCircle = function(c4Name)
    {
        return c4Name;
    };



    c4StateHandler = function (state) {
        mxVertexHandler.apply(this, arguments);
    };
    c4StateHandler.prototype = new mxVertexHandler();
    c4StateHandler.prototype.constructor = c4StateHandler;
    c4StateHandler.prototype.domNode = null;
    c4StateHandler.prototype.init = function () {
        mxVertexHandler.prototype.init.apply(this, arguments);
        this.domNode = document.createElement('div');
        this.domNode.style.position = 'absolute';
        this.domNode.style.whiteSpace = 'nowrap';
        if (this.custom) this.custom.apply(this, arguments);
        var img = c4Utils.createSettingsIcon();
        mxEvent.addGestureListeners(img,
            mxUtils.bind(this, function (evt) {
                mxEvent.consume(evt);
            })
        );
        mxEvent.addListener(img, 'click',
            mxUtils.bind(this, function (evt) {
                var isC4Person = c4Utils.isC4Person(this.state.cell);

                if (isC4Person) {
                    var bodyCell = this.state.cell.getChildAt(2);
                    var iconCell = this.state.cell.getChildAt(1);

                    if (bodyCell !== null) {
                        var dlg = new EditDataDialog(ui, bodyCell, iconCell);
                        ui.showDialog(dlg.container, 320, 320, true, false);
                        dlg.init();
                    }
                }
                if (!isC4Person) {
                    ui.actions.get('editData').funct();
                }
                mxEvent.consume(evt);
            })
        );
        this.domNode.appendChild(img);
        this.graph.container.appendChild(this.domNode);
        this.redrawTools();
    };
    c4StateHandler.prototype.redraw = function () {
        mxVertexHandler.prototype.redraw.apply(this);
        this.redrawTools();
    };
    c4StateHandler.prototype.redrawTools = function () {
        if (this.state !== null && this.domNode !== null) {
            var dy = (mxClient.IS_VML && document.compatMode === 'CSS1Compat') ? 20 : 4;
            this.domNode.style.left = (this.state.x + this.state.width - this.domNode.children.length * 14) + 'px';
            this.domNode.style.top = (this.state.y + this.state.height + dy) + 'px';
        }
    };
    c4StateHandler.prototype.destroy = function (sender, me) {
        mxVertexHandler.prototype.destroy.apply(this, arguments);
        if (this.domNode !== null) {
            this.domNode.parentNode.removeChild(this.domNode);
            this.domNode = null;
        }
    };

    // ###############
    // ## C4 PERSON ##
    // ###############
    C4Person = function () {
    };
    C4Person.prototype.handler = c4StateHandler;
    C4Person.prototype.create = function (color) {

        var group = new mxCell('', new mxGeometry(0, 0, 190, 113), 'group;rounded=0;labelBackgroundColor=none;fillColor=none;fontColor=#000000;align=center;html=1;');
        group.setVertex(true);
        group.setConnectable(false);
        group.setAttribute('c4Type', 'person');
        group.c4 = this;

        var body = new mxCell('', new mxGeometry(0, 33, 190, 80), 'rounded=1;whiteSpace=wrap;fillColor=' + color + ';fontFamily=Open Sans;arcSize=3;align=center;spacingLeft=0;verticalAlign=top;fontColor=#FFFFFF;strokeColor=none;shadow=0;comic=0;glass=0;FType=g;html=1;');
        body.setParent(group);
        body.setVertex(true);
        body.setValue(mxUtils.createXmlDocument().createElement('object'));
        body.setAttribute('label', c4Utils.labelPerson('Name', 'Description'));
        body.setAttribute('placeholders', '1');
        body.setAttribute('c4Name', 'Name');
        body.setAttribute('c4Type', 'body');
        body.setAttribute('c4Description', 'Description');
        body.c4 = this;

        var head = new mxCell('', new mxGeometry(70, 0, 50, 50), 'ellipse;whiteSpace=wrap;html=1;aspect=fixed;rounded=0;labelBackgroundColor=none;fillColor=' + color + ';fontSize=12;fontColor=#000000;align=center;strokeColor=none;');
        head.setParent(group);
        head.setVertex(true);
        head.setAttribute('c4Type', 'head');
        head.c4 = this;

        var icon = new mxCell('', new mxGeometry(83, 11, 23, 26), 'shadow=0;dashed=0;html=1;strokeColor=none;fillColor=#FFFFFF;labelPosition=center;verticalLabelPosition=bottom;verticalAlign=top;align=center;outlineConnect=0;shape=mxgraph.veeam.2d.group;fontFamily=Tahoma;fontSize=16;fontColor=#000000;aspect=fixed;');
        icon.setParent(group);
        icon.setVertex(true);
        icon.setAttribute('c4Type', 'icon');
        icon.c4 = this;

        group.insert(body); // child: 2 !!
        group.insert(head);
        group.insert(icon);
        group.label = 'C4 Person';
        return group;
    };
    c4Utils.registCodec(C4Person);

    // ######################
    // ## C4 PERSON CIRCLE ##
    // ######################
    C4PersonCircle = function () {
    };
    C4PersonCircle.prototype.handler = c4StateHandler;
    C4PersonCircle.prototype.create = function (color) {
        var c4PersonCircle = new mxCell('', new mxGeometry(0, 0, 70, 70), 'ellipse;whiteSpace=wrap;html=1;aspect=fixed;fillColor=' + color + ';fontFamily=Open Sans;FType=g;fontStyle=1;fontSize=12;fontColor=#FFFFFF;strokeColor=none;');
        c4PersonCircle.setVertex(true);
        c4PersonCircle.setValue(mxUtils.createXmlDocument().createElement('object'));
        c4PersonCircle.setAttribute('label', c4Utils.labelPersonCircle('Name'));
        c4PersonCircle.setAttribute('placeholders', '1');
        c4PersonCircle.setAttribute('c4Name', 'Name');
        c4PersonCircle.setAttribute('c4Type', 'PersonCircle');
        c4PersonCircle.c4 = this;
        c4PersonCircle.label = 'C4 Person Circle';
        return c4PersonCircle;
    };
    c4Utils.registCodec(C4PersonCircle);

    // ########################
    // ## C4 SOFTWARE SYSTEM ##
    // ########################
    C4SoftwareSystem = function () {
    };
    C4SoftwareSystem.prototype.handler = c4StateHandler;
    C4SoftwareSystem.prototype.create = function (color) {
        var c4SoftwareSystem = new mxCell('', new mxGeometry(0, 70, 190, 100), 'rounded=1;whiteSpace=wrap;fillColor=' + color + ';fontFamily=Open Sans;arcSize=3;align=center;spacingLeft=0;verticalAlign=top;fontColor=#FFFFFF;strokeColor=none;shadow=0;comic=0;glass=0;FType=g;html=1;');
        c4SoftwareSystem.setVertex(true);
        c4SoftwareSystem.setValue(mxUtils.createXmlDocument().createElement('object'));
        c4SoftwareSystem.setAttribute('label', c4Utils.labelSoftwareSystem('Name', '', 'Description'));
        c4SoftwareSystem.setAttribute('placeholders', '1');
        c4SoftwareSystem.setAttribute('c4Name', 'Name');
        c4SoftwareSystem.setAttribute('c4Type', 'SoftwareSystem');
        c4SoftwareSystem.setAttribute('c4Technology', '');
        c4SoftwareSystem.setAttribute('c4Description', 'Description');
        c4SoftwareSystem.c4 = this;
        c4SoftwareSystem.label = 'C4 Software System';
        return c4SoftwareSystem;
    };
    c4Utils.registCodec(C4SoftwareSystem);

    // ##################
    // ## C4 CONTAINER ##
    // ##################
    C4Container = function () {
    };
    C4Container.prototype.handler = c4StateHandler;
    C4Container.prototype.create = function (color) {
        var c4Container = new mxCell('', new mxGeometry(0, 70, 190, 100), 'rounded=1;whiteSpace=wrap;fillColor=' + color + ';fontFamily=Open Sans;arcSize=3;align=center;spacingLeft=0;verticalAlign=top;fontColor=#FFFFFF;strokeColor=none;shadow=0;comic=0;glass=0;FType=g;html=1;');
        c4Container.setVertex(true);
        c4Container.setValue(mxUtils.createXmlDocument().createElement('object'));
        c4Container.setAttribute('label', c4Utils.labelContainer('Name', 'Technology', 'Description'));
        c4Container.setAttribute('placeholders', '1');
        c4Container.setAttribute('c4Name', 'Name');
        c4Container.setAttribute('c4Type', 'Container');
        c4Container.setAttribute('c4Technology', 'Technology');
        c4Container.setAttribute('c4Description', 'Description');
        c4Container.c4 = this;
        c4Container.label = 'C4 Container';
        return c4Container;
    };
    c4Utils.registCodec(C4Container);

    // ###################
    // ## C4 COMPONTENT ##
    // ###################
    C4Component = function () {
    };
    C4Component.prototype.handler = c4StateHandler;
    C4Component.prototype.create = function (color) {
        var c4Component = new mxCell('', new mxGeometry(0, 70, 190, 100), 'rounded=1;whiteSpace=wrap;fillColor=' + color + ';fontFamily=Open Sans;arcSize=3;align=center;spacingLeft=0;verticalAlign=top;fontColor=#FFFFFF;strokeColor=none;shadow=0;comic=0;glass=0;FType=g;html=1;');
        c4Component.setVertex(true);
        c4Component.setValue(mxUtils.createXmlDocument().createElement('object'));
        c4Component.setAttribute('label', c4Utils.labelComponent('Name', 'Technology', 'Description'));
        c4Component.setAttribute('placeholders', '1');
        c4Component.setAttribute('c4Name', 'Name');
        c4Component.setAttribute('c4Type', 'Component');
        c4Component.setAttribute('c4Technology', 'Technology');
        c4Component.setAttribute('c4Description', 'Description');
        c4Component.c4 = this;
        c4Component.label = 'C4 Component';
        return c4Component;
    };
    c4Utils.registCodec(C4Component);

    // ##############################
    // ## C4 EXECUTION ENVIRONMENT ##
    // ##############################
    C4ExecutionEnvironment = function () {
    };
    C4ExecutionEnvironment.prototype.handler = c4StateHandler;
    C4ExecutionEnvironment.prototype.create = function () {
        var c4ExecutionEnvironment = new mxCell('', new mxGeometry(0, 70, 200, 170), 'rounded=1;whiteSpace=wrap;html=1;fillColor=#ffffff;fontFamily=Open Sans;fontColor=#000000;align=left;arcSize=3;verticalAlign=top;fontSize=12;fontStyle=1;movable=1;resizable=1;rotatable=1;deletable=1;editable=1;connectable=1;spacingRight=0;strokeWidth=1;glass=0;comic=0;shadow=0;spacingLeft=5;spacingBottom=5;strokeColor=#666666;');
        c4ExecutionEnvironment.setVertex(true);
        c4ExecutionEnvironment.setValue(mxUtils.createXmlDocument().createElement('object'));
        c4ExecutionEnvironment.setAttribute('label', c4Utils.labelExecutionEnvironment('Name', 'Application'));
        c4ExecutionEnvironment.setAttribute('placeholders', '1');
        c4ExecutionEnvironment.setAttribute('c4Name', 'Name');
        c4ExecutionEnvironment.setAttribute('c4Type', 'ExecutionEnvironment');
        c4ExecutionEnvironment.setAttribute('c4Application', 'Application');
        c4ExecutionEnvironment.c4 = this;
        c4ExecutionEnvironment.label = 'C4 Execution Environment';
        return c4ExecutionEnvironment;
    };
    c4Utils.registCodec(C4ExecutionEnvironment);

    // ########################
    // ## C4 DEPLOYMENT NODE ##
    // ########################
    C4DeploymentNode = function () {
    };
    C4DeploymentNode.prototype.handler = c4StateHandler;
    C4DeploymentNode.prototype.create = function () {
        var c4DeploymentNode = new mxCell('', new mxGeometry(0, 70, 240, 230), 'rounded=1;whiteSpace=wrap;html=1;labelBackgroundColor=none;fillColor=#ffffff;fontColor=#000000;align=left;arcSize=3;strokeColor=#000000;verticalAlign=bottom;');
        c4DeploymentNode.setVertex(true);
        c4DeploymentNode.setValue(mxUtils.createXmlDocument().createElement('object'));
        c4DeploymentNode.setAttribute('label', c4Utils.labelDeploymentNode('Hostname', 'OperationSystem', 'ScalingFactor'));
        c4DeploymentNode.setAttribute('placeholders', '1');
        c4DeploymentNode.setAttribute('c4Name', 'Hostname');
        c4DeploymentNode.setAttribute('c4Type', 'DeploymentNode');
        c4DeploymentNode.setAttribute('c4OperationSystem', 'OperationSystem');
        c4DeploymentNode.setAttribute('c4ScalingFactor', 'ScalingFactor');
        c4DeploymentNode.c4 = this;
        c4DeploymentNode.label = 'C4 Deployment Node';
        return c4DeploymentNode;
    };
    c4Utils.registCodec(C4DeploymentNode);

    // #####################
    // ## C4 RELATIONSHIP ##
    // #####################
    C4Relationship = function () {
    };
    C4Relationship.prototype.handler = c4StateHandler;
    C4Relationship.prototype.create = function () {
        var label = c4Utils.labelRelationship('Description', 'Technology');
        var cell = new mxCell('', new mxGeometry(0, 0, 160, 0), 'endArrow=none;dashed=1;html=1;fontFamily=Open Sans;fontColor=#000000;strokeWidth=2;strokeColor=#000000;fontStyle=1;startArrow=block;startFill=1;');
        cell.setValue(mxUtils.createXmlDocument().createElement('object'));
        cell.geometry.setTerminalPoint(new mxPoint(0, 0), true);
        cell.geometry.setTerminalPoint(new mxPoint(160, 0), false);
        cell.geometry.relative = true;
        cell.edge = true;
        cell.value.setAttribute('label', label);
        cell.value.setAttribute('c4Type', 'Relationship');
        cell.value.setAttribute('c4Description', 'Description');
        cell.value.setAttribute('c4Technology', 'Technology');
        cell.c4 = this;
        return cell;
    };
    c4Utils.registCodec(C4Relationship);

    // Adds custom sidebar entry
    ui.sidebar.addPalette(sidebar_id, sidebar_title, true, function (content) {
        var backgroundColors = ["#000000", "#1F4D59", "#58C0B3", "#94D9D3", "#E34647", "#DBB03C", "#1A18F7"];
        var verticies = [C4ExecutionEnvironment];
        var verticiesColors = [C4SoftwareSystem, C4Container, C4Component, C4Person, C4PersonCircle];

        for (var i in verticiesColors) {
            for(var j in backgroundColors)
            {
                var cell = verticiesColors[i].prototype.create(backgroundColors[j]);
                content.appendChild(ui.sidebar.createVertexTemplateFromCells([cell], cell.geometry.width, cell.geometry.height, cell.label));
            }
        }

        for (var i in verticies) {
            var cell = verticies[i].prototype.create();
            content.appendChild(ui.sidebar.createVertexTemplateFromCells([cell], cell.geometry.width, cell.geometry.height, cell.label));
        }

        content.appendChild(ui.sidebar.createEdgeTemplateFromCells([C4Relationship.prototype.create()], 160, 0, 'C4 Relationship'));
        // , C4DynamicRelationship];
    });

    // Add custom handler-code for the event of data-editor instanzing to provide a custom data-editor dialog.
    origGraphCreateHander = ui.editor.graph.createHandler;
    ui.editor.graph.createHandler = function (state) {
        if (state !== null && (this.getSelectionCells().length === 1) && c4Utils.isC4(state.cell) && state.cell.c4.handler
            && !c4Utils.isC4Relationship(state.cell)) {
            return new state.cell.c4.handler(state);
        }
        return origGraphCreateHander.apply(this, arguments);
    };

    // START -> CUSTOM EDITOR MENU!
    origEditDataDialog = EditDataDialog;
    EditDataDialog = function (ui, cell, iconCell)
    {
        if (!c4Utils.isC4(cell))
        {
            return origEditDataDialog.apply(this, arguments);
        }

        var div = document.createElement('div');
        var graph = ui.editor ? ui.editor.graph : ui.graph;
        div.style.height = '100%'; //'310px';
        div.style.overflow = 'auto';

        // Converts the values to an XML node
        var value = graph.getModel().getValue(cell);
        if (!mxUtils.isNode(value)) {
            var obj = mxUtils.createXmlDocument().createElement('object');
            obj.setAttribute('label', value || '');
            value = obj;
        }

        // Creates the dialog contents
        var form = new mxForm('properties');
        form.table.style.width = '100%';
        form.table.style.paddingRight = '20px';

        var colgroupName = document.createElement('colgroup');
        colgroupName.width = '120';

        form.table.insertBefore(colgroupName, form.body);
        form.table.insertBefore(document.createElement('colgroup'), form.body);

        var attrs = value.attributes;
        var names = [];
        var texts = [];
        var count = 0;

        var addTextArea = function (index, name, value, tabIndex) {
            names[index] = name;
            texts[index] = form.addTextarea(names[count] + ':', value, 2);
            texts[index].style.width = '100%';
            texts[index].tabIndex = tabIndex;
            return texts[index];
        };

        var addAttribute = function (index, name, value) {
            names[index] = name;
            texts[index] = document.createElement('textarea');
            texts[index].value = value;
            return texts[index];
        };

        var addIconSelect = function (index, name, value) {
            names[index] = name;
            texts[index] = form.addCombo(names[count] + ':', false, 0);

            var items = [
                "--no image--|",
                "Kubernetes|mxgraph.kubernetes.icon",
                "Blob|mxgraph.azure.storage_blob",
                "Database|mxgraph.mscae.enterprise.database_generic",
                "Website|mxgraph.azure.azure_website",
                "Person|mxgraph.veeam.2d.group"
            ];

            items.forEach( function(item) {
                var values = item.split('|');
                form.addOption(texts[index], values[0], values[1], values[1] === value);
            });

            return texts[index];
        };

        var addColorSelect = function (index, name, value) {
            names[index] = name;
            texts[index] = form.addCombo(names[count] + ':', false, 0);

            var items = [
                "--no image--|",
                "Kubernetes|mxgraph.kubernetes.icon",
                "Blob|mxgraph.azure.storage_blob",
                "Database|mxgraph.mscae.enterprise.database_generic",
                "Website|mxgraph.azure.azure_website",
                "Person|mxgraph.veeam.2d.group"
            ];

            items.forEach( function(item) {
                var values = item.split('|');
                form.addOption(texts[index], values[0], values[1], values[1] === value);
            });

            return texts[index];
        };

        for (var i = 0; i < attrs.length; i++) {
            var nodeName = attrs[i].nodeName;
            var nodeValue = attrs[i].nodeValue;
            // if (cell.awssf.hiddenAttributes && cell.awssf.hiddenAttributes.indexOf(nodeName) >= 0) continue;
            if (nodeName === 'c4Type')
            {
                // type
                var span = document.createElement('span');
                mxUtils.write(span, nodeValue);
                form.addField('c4Type:', span);
            }
            else if (nodeName === 'label')
            {
                // summary
                addAttribute(count, nodeName, nodeValue);
                count++;
                var labelDiv = document.createElement('div');
                labelDiv.setAttribute('style', 'border: 1px dashed #c2c2c2; margin-bottom: 4px;');
                labelDiv.innerHTML = nodeValue;
                div.appendChild(labelDiv);
            }
            else if (nodeName === 'c4Icon')
            {
                addIconSelect(count, nodeName, nodeValue, i);
                count++;
            }
            else if (nodeName !== 'placeholders')
            {
                // others
                addTextArea(count, nodeName, nodeValue, i);
                count++;
            }
        }
        div.appendChild(form.table);
        this.init = function ()
        {
            function getIndexOfC4Type(c4type)
            {
                for (var i = 0; i < names.length; i++)
                {
                    if (names[i] === c4type) //'c4Name'
                    {
                        return i;
                    }
                }
            }

            var firstInputField = -1;
            switch (cell.getAttribute('c4Type')) {
                case 'body':
                case 'SoftwareSystem':
                case 'Container':
                case 'containerBody':
                case 'Component':
                case 'ExecutionEnvironment':
                case 'DeploymentNode':
                    firstInputField = getIndexOfC4Type('c4Name');
                    break;
                case 'Relationship':
                    firstInputField = getIndexOfC4Type('c4Description');
                    break;
                case 'DynamicRelationship':
                    firstInputField = getIndexOfC4Type('c4Step');
                    break;
                case 'Database':
                    firstInputField = getIndexOfC4Type('c4Technology');
                    break;
            }
            if (texts.length > 0 && firstInputField !== -1) {
                texts[firstInputField].focus();
            }
        };

        // cancel button
        var cancelBtn = mxUtils.button(mxResources.get('cancel'), function () {
            ui.hideDialog.apply(ui, arguments);
        });
        cancelBtn.className = 'geBtn';

        // apply button
        var applyBtn = mxUtils.button(mxResources.get('apply'), function () {
            try {
                ui.hideDialog.apply(ui, arguments);
                // Clones and updates the value
                value = value.cloneNode(true);
                var removeLabel = false;
                var c4Icon = '';

                var c4NotationUpdate = function () {
                    var c4Name = '';
                    var c4Description = '';
                    var c4Technology = '';
                    var c4OperationSystem = '';
                    var c4Application = '';
                    var c4ScalingFactor = '';
                    var c4Step = 1;
                    var labelIndex = -1;

                    for (var i = 0; i < names.length; i++) {
                        if (names[i] === 'c4Name') {
                            c4Name = texts[i].value;
                        }
                        if (names[i] === 'c4Description') {
                            c4Description = texts[i].value;
                        }
                        if (names[i] === 'c4Technology') {
                            c4Technology = texts[i].value;
                        }
                        if (names[i] === 'c4OperationSystem') {
                            c4OperationSystem = texts[i].value;
                        }
                        if (names[i] === 'c4Application') {
                            c4Application = texts[i].value;
                        }
                        if (names[i] === 'c4ScalingFactor') {
                            c4ScalingFactor = texts[i].value;
                        }
                        if (names[i] === 'c4Step') {
                            c4Step = texts[i].value;
                        }
                        if (names[i] === 'c4Icon') {
                            c4Icon = texts[i].value;
                        }
                        if (names[i] === 'label') {
                            labelIndex = i;
                        }
                    }
                    if (labelIndex >= 0) {
                        switch (cell.getAttribute('c4Type')) {
                            case 'body':
                                texts[labelIndex].value = c4Utils.labelPerson(c4Name, c4Description);
                                break;
                            case 'PersonCircle':
                                texts[labelIndex].value = c4Utils.labelPersonCircle(c4Name);
                                break;
                            case 'SoftwareSystem':
                                texts[labelIndex].value = c4Utils.labelSoftwareSystem(c4Name, c4Technology, c4Description);
                                break;
                            case 'Container':
                            case 'containerBody':
                                texts[labelIndex].value = c4Utils.labelContainer(c4Name, c4Technology, c4Description);
                                break;
                            case 'Component':
                                texts[labelIndex].value = c4Utils.labelComponent(c4Name, c4Technology, c4Description);
                                break;
                            case 'Relationship':
                                texts[labelIndex].value = c4Utils.labelRelationship(c4Description, c4Technology);
                                break;
                            case 'ExecutionEnvironment':
                                texts[labelIndex].value = c4Utils.labelExecutionEnvironment(c4Name, c4Application);
                                break;
                            case 'DeploymentNode':
                                texts[labelIndex].value = c4Utils.labelDeploymentNode(c4Name, c4OperationSystem, c4ScalingFactor);
                                break;
                            case 'DynamicRelationship':
                                texts[labelIndex].value = c4Utils.labelDynamicRelationship(c4Step, c4Description, c4Technology);
                                break;
                            case 'Database':
                                texts[labelIndex].value = c4Utils.labelDatabase(c4Technology, c4Description);
                                break;
                        }
                    }
                }();

                for (var i = 0; i < names.length; i++) {
                    if (cell.c4 && cell.c4.applyForm)
                    {
                        removeLabel = removeLabel || cell.c4.applyForm(value, names[i], texts[i]);
                    }
                    else
                    {
                        if (texts[i] === null)
                        {
                            value.removeAttribute(names[i]);
                        }
                        else
                        {
                            value.setAttribute(names[i], texts[i].value);
                            removeLabel = removeLabel || (names[i] === 'placeholder' && value.getAttribute('placeholders') === '1');
                        }
                    }
                }

                // Removes label if placeholder is assigned
                if (removeLabel) {
                    value.removeAttribute('label');
                }

                // Updates the value of the cell (undoable)
                graph.getModel().setValue(cell, value);

                if (iconCell && c4Icon)
                {
                    var newIconStyle = c4Utils.cellStyleIcon(c4Icon);
                    graph.getModel().setStyle(iconCell, newIconStyle);
                }
            }
            catch (e) {
                mxUtils.alert(e);
            }
        });
        applyBtn.className = 'geBtn gePrimaryBtn';
        applyBtn.tabIndex = 10;
        var buttons = document.createElement('div');
        buttons.style.marginTop = '18px';
        buttons.style.textAlign = 'right';

        if (graph.getModel().isVertex(cell) || graph.getModel().isEdge(cell)) {
            var replace = document.createElement('span');
            replace.style.marginRight = '10px';
            var input = document.createElement('input');
            input.setAttribute('type', 'checkbox');
            input.style.marginRight = '6px';
            if (value.getAttribute('placeholders') === '1') {
                input.setAttribute('checked', 'checked');
                input.defaultChecked = true;
            }
            mxEvent.addListener(input, 'click', function () {
                if (value.getAttribute('placeholders') === '1') {
                    value.removeAttribute('placeholders');
                }
                else {
                    value.setAttribute('placeholders', '1');
                }
            });
            replace.appendChild(input);
            mxUtils.write(replace, mxResources.get('placeholders'));
            if (EditDataDialog.placeholderHelpLink !== null) {
                var createHelpIcon = function () {
                    var link = document.createElement('a');
                    link.setAttribute('href', EditDataDialog.placeholderHelpLink);
                    link.setAttribute('title', mxResources.get('help'));
                    link.setAttribute('target', '_blank');
                    link.style.marginLeft = '10px';
                    link.style.cursor = 'help';
                    var icon = document.createElement('img');
                    icon.setAttribute('border', '0');
                    icon.setAttribute('valign', 'middle');
                    icon.style.marginTop = '-4px';
                    icon.setAttribute('src', Editor.helpImage);
                    link.appendChild(icon);
                    replace.appendChild(link);
                }();
            }
            buttons.appendChild(replace);
        }
        if (ui.editor && ui.editor.cancelFirst) {
            buttons.appendChild(cancelBtn);
            buttons.appendChild(applyBtn);
        }
        else {
            buttons.appendChild(applyBtn);
            buttons.appendChild(cancelBtn);
        }
        div.appendChild(buttons);
        this.container = div;
    }
    ///// END <- CUSTOM EDITOR FORM
});