## C4 Modelling little bit easier (c4-diagrams.io)
Original:
Github-Pages on: https://tobiashochguertel.github.io/c4-draw.io/ (which is the same as the README.adoc file, but I think it’s better to read there…​)

### About
You can make C4 Architecture Software System Models with driagrams.io.

This is a C4 Modelling plugin for driagrams.io, which provides C4 Notation Elements in driagrams.io.

### Quick Start
1. Access to https://www.driagrams.io/.
2. Save the drawing, or decide later: File → Save → Option
3. Open the plugin dialog: Extras → Plugins
4. Open the add plugin dialog with an click: Add
5. Put in the field Enter Value (URL): the link to the plugin: https://bitbucket.org/tamtam-nl/dept-c4-diagrams.io/downloads/c4.js
6. Click on Apply
7. Reload the page! to see the C4 Notation Shapes in the shape panel (figure 1).

After the reload, the C4 Notation Shapes are available from the shape panel:

![C4NotationShapes](img/C4NotationShapes.png)

### Usage

1. Drag a C4 notation shape on the diagram paper
2. The most C4 notation shapes provide an small gear icon when they are selected. (_figure 3_)
3. Edit the Properties of the selected C4 Notation Shape: (_figure 4_)
   1. Click the small gear to open the _properties dialog_ (_figure 3_)
   2. Press the key-stroke <kbd>CMD</kbd> + <kbd>M</kbd> to open the _properties dialog_ (_figure 3_)
   3. ,or use the menu: <kbd>Edit</kbd> -> <kbd>Edit Data (CMD</kbd> + <kbd>M)</kbd> (_figure 6_)
4. Input Cursor / Focus is on the first field of the dialog, and with <kbd>tab</kbd> you can jump through the fields and submit (_figure 5_).

.figure 3: Location of the small gear, to open the properties dialog:<br />
![small-gear-on-c4-person-shape.png](img/small-gear-on-c4-person-shape.png)


.figure 4: Example of properties dialog of an C4-Person notation shape:<br />
![Data-Editor-Dialog-to-set-the-properties.png](img/Data-Editor-Dialog-to-set-the-properties.png)


.figure 5: Control tab order of the properties fields:<br />
![Control-Tab-Order-of-C4-Notation-Shape-Properties-Dialog.png](img/Control-Tab-Order-of-C4-Notation-Shape-Properties-Dialog.png)


.figure 6: Other way to open the properties dialog:<br />
![Open-the-Properties-Dialog-from-Menu-bar.png](img/Open-the-Properties-Dialog-from-Menu-bar.png)
